package com.amydegregorio.javabasics.operators;

public class AssignmentDemo {

   public static void main(String[] args) {
       AssignmentDemo demo = new AssignmentDemo();
       demo.compoundArithmeticDemo();
       System.out.println();
       demo.unaryDemo();
   }
   
   public void compoundArithmeticDemo() {
      int x = 5;
      System.out.println("x = " + x);
      x += 6;
      System.out.println("After x += 6, x = " + x);
      
      x -= 7;
      System.out.println("After x -= 7, x = " + x);
      
      x *= 10;
      System.out.println("After x *= 10, x = " + x);
      
      x /= 10;
      System.out.println("After x /= 5, x = " + x);
      
      x %= 2;
      System.out.println("After x %= 2, x = " + x);
   }
   
   public void unaryDemo() {
      int i = 0;
      System.out.println("i++ = " + i++ + ", final i = " + i);
      
      i = 0;
      System.out.println("++i = " + ++i + ", final i = " + i);
      
      i = 5;
      System.out.println("i-- = " + i-- + ", final i = " + i);
      
      i = 5;
      System.out.println("--i = " + --i + ", final i = " + i);
      
      boolean isOn = true;
      System.out.println("isOn = " + isOn);
      System.out.println("inverted isOn = " + !isOn);
   }

}
