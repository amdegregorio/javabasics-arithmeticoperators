# JavaBasics-ArithmeticOperators

Example code for Java Basics: Arithmetic Operators article.

For more information see [Java Basics: Arithmetic Operators](http://amydegregorio.com/2020/01/09/java-basics-arithmetic-operators/)

To run, run the `AssignmentDemo` class.

#### License

This project is licensed under the Apache License version 2.0.  
See the LICENSE file or go to [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0) for more information. 
